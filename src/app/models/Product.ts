interface Product {
  title: string;
  image: string;
  price: number;
}

export default Product;

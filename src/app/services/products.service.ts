import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Product from '../models/Product';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private readonly http: HttpClient) {}

  public getProducts() {
    return this.http.get<Product[]>('https://fakestoreapi.com/products');
  }
}
